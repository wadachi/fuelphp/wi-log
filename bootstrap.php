<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Package::load([
  'wi-container',
  'wi-migration',
  'wi-model',
  'wi-validation',
]);

\Autoloader::add_classes([
  'Wi\\Interface_Service_Log' => __DIR__.'/classes/interface/service/log.php',
  'Wi\\Model_Log_Entries' => __DIR__.'/classes/model/log/entries.php',
  'Wi\\Service_Log' => __DIR__.'/classes/service/log.php',
]);
