# Wadachi FuelPHP Package

Services for logging.

## Periodic Maintenance

### Cron Job

Open the Cron table editor:
```bash
crontab -e
```

Configure the maintenance task;
```
# ログのため定期保守 タスク
@hourly /usr/bin/php /home/app/oil r logs; touch /tmp/logs.task
```

Each time the task is run, the date and time will be stamped on the task file at */tmp/logs.task*.
