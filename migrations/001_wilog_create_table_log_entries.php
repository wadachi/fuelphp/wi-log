<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Migrations;

class WiLog_Create_Table_Log_Entries extends \Wi\Migration
{
  function up()
  {
    \Config::load('wi-log', true);
    $connection = \Config::get('wi-log.maintenance.connection');

    \DBUtil::create_table('log_entries', [
      'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'comment' => 'ID'],
      'log' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'ログ名'],
      'level' => ['type' => 'varchar', 'constraint' => 11, 'comment' => 'レベル'],
      'date' => ['type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP'), 'comment' => '日時'],
      'user_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'comment' => 'ユーザーID'],
      'source' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'ソース名'],
      'event' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'イベント名'],
      'result' => ['type' => 'varchar', 'constraint' => 255, 'null' => true, 'comment' => '結果'],
      'message' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'メッセージ'],
      'data' => ['type' => 'text', 'null' => true, 'comment' => 'データ'],
    ], ['id'], false, 'InnoDB', 'utf8_unicode_ci');  // !!! utf8_unicode_ci A = a = Ａ

    \DBUtil::create_index('log_entries', ['date'], 'date');

    $this->execute_sql([
      "GRANT SELECT, INSERT ON TABLE `log_entries` TO '".\Config::get('db.default.connection.username')."'@'localhost'",
      "GRANT SELECT, DELETE ON TABLE `log_entries` TO '".\Config::get('db.'.$connection.'.connection.username')."'@'localhost'",
    ]);
  }

  function down()
  {
    \Config::load('wi-log', true);
    $connection = \Config::get('wi-log.maintenance.connection');

    $this->execute_sql([
      "GRANT SELECT, DELETE ON TABLE `log_entries` TO '".\Config::get('db.'.$connection.'.connection.username')."'@'localhost'",
      "REVOKE SELECT, DELETE ON TABLE `log_entries` FROM '".\Config::get('db.'.$connection.'.connection.username')."'@'localhost'",
      "GRANT SELECT, INSERT ON TABLE `log_entries` TO '".\Config::get('db.default.connection.username')."'@'localhost'",
      "REVOKE SELECT, INSERT ON TABLE `log_entries` FROM '".\Config::get('db.default.connection.username')."'@'localhost'",
    ]);

    \DBUtil::drop_index('log_entries', 'date');
    \DBUtil::drop_table('log_entries');
  }
}
