<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Tasks;

/**
 * ログ タスク
 */
class Logs
{
  // サービス
  private static $log_service;

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    // データベース接続設定
    \Config::load('db', true);
    \Config::load('wi-log', true);
    $connection_name = \Config::get('wi-log.maintenance.connection');
    $connection = \Config::get('db.'.$connection_name);
    \Config::set('db.default', $connection);

    // ログ　サーブす
    static::$log_service = \Wi\Container::get('service.log');
  }// </editor-fold>

  /**
   * ログ処理を実行する
   * 実行の方法：php oil r logs
   *
   * @access public
   */
  public static function run()// <editor-fold defaultstate="collapsed" desc="...">
  {
    // 診断法
    \Cli::write('FUEL_ENV: '.\Fuel::$env);

    \DB::start_transaction();
    try
    {
      static::prune();
      \DB::commit_transaction();
    }
    catch (\Exception $e)
    {
      \DB::rollback_transaction();
      var_dump($e);
    }

    return 0;
  }// </editor-fold>


  /**
   * ログを剪定する
   * 実行の方法：php oil r logs:prune
   *
   * @access public
   */
  public static function prune()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $logs = static::$log_service->find_logs();

    foreach ($logs['rows'] as $log)
    {
      static::$log_service->prune($log['name']);
    }
  }// </editor-fold>

  /**
   * ログを消去する
   * 実行の方法：php oil r logs:purge
   *
   * @access public
   */
  public static function purge()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $logs = static::$log_service->find_logs();

    foreach ($logs['rows'] as $log)
    {
      static::$log_service->prune($log['name'], ['max_entries' => 0]);
    }
  }// </editor-fold>
}
