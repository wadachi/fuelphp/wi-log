<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * ログ設定
 */
return [
  /*
   * デフォルト　ログ
   */
  'log' => 'Application',

  /*
   * デフォルト　ログ　レベル
   */
  'level' => 'error',

  /*
   * デフォルト　ログ　ソース
   */
  'source' => 'Unspecified',

  /*
   * ログの保守設定
   */
  'maintenance' => [
    /*
     * 接続名
     */
    'connection' => 'default',

    /*
     * ログ項目の最大有効期間
     * (-1 = 未施行)
     */
    'max_age' => 90,

    /*
     * ログ項目の最大数
     * (-1 = 未施行)
     */
    'max_entries' => 1000,
  ],
];
