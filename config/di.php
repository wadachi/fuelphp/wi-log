<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 依存性注入コンテナの設定
 */
return [
  'service.log' => [
    'class' => 'Wi\\Service_Log',
  ],
];
