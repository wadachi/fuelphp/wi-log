<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * ログ設定
 */
return [
  'level' => 'verbose',
];
