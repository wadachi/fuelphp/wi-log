<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ログ レーベル 区分
 */
abstract class LogLevels
{
  const None = 'none';
  const Warn = 'warning';
  const Verbose = 'verbose';
  const Info = 'information';
  const Error = 'error';
  const Critical = 'critical';
}

/**
 * ログ サービス インタフェース
 */
interface Interface_Service_Log
{
  /**
   * ログ項目を検索する
   *
   * @access public
   * @param string log ログ名
   * @param array $criteria 検索条件
   *        string level レベル
   *        datetime date_from 日から
   *        datetime date_to まで日
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   */
  function find_entries($log, array $criteria, array $options);

  /**
   * ログを検索する
   *
   * @access public
   * @param array $criteria 検索条件
   *        string name ログ名
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   */
  function find_logs(array $criteria, array $options);

  /**
   * ログ項目を取得する
   *
   * @access public
   * @param string $log ログ名
   * @param integer $id 項目ID
   * @return array ログ
   */
  function get_entry($log, $id);

  /**
   * ログを剪定する
   *
   * @access public
   * @param string $log ログ名
   * @param mixed $config 設定
   * @return array ログ
   */
  function prune($log, $config);

  /**
   * ログ項目を保存する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   * @return array ログ項目
   */
  function save_entry($log, array $properties);

  /**
   * ログ項目の検索プロパティを検証する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date_start 日から
   *        datetime date_end まで日
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  function validate_find_entries($log, array $properties);

  /**
   * ログの検索プロパティを検証する
   *
   * @access public
   * @param array $properties プロパティ
   *        string name ログ名
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  function validate_find_logs(array $properties);

  /**
   * ログ項目保存のプロパティを検証する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  function validate_save_entry($log, array $properties);

  /**
   * 設定のログレベルによると、ログ項目を追加する
   *
   * @access protected
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   */
  function write($log, array $properties);
}
