<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 監査ログ モデル
 */
class Model_Log_Entries extends Model
{
  // テーブル名
  protected static $_table_name = 'log_entries';

  // 保存プロパティ
  protected static $_properties = [
    'id' => [
      'data_type' => 'int',
    ],

    'log' => [
      'data_type' => 'varchar',
    ],

    'level' => [
      'data_type' => 'char',
    ],

    'date' => [
      'data_type' => 'time_mysql'
    ],

    'user_id' => [
      'data_type' => 'varchar'
    ],

    'source' => [
      'data_type' => 'varchar'
    ],

    'event' => [
      'data_type' => 'varchar'
    ],

    'result' => [
      'data_type' => 'varchar'
    ],

    'message' => [
      'data_type' => 'varchar'
    ],

    'data' => [
      'data_type' => 'text'
    ],
  ];
}
