<?php
/**
 * Wadachi FuelPHP Log Package
 *
 * Services for logging.
 *
 * @package    wi-log
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ログ サービス
 */
class Service_Log extends Service implements Interface_Service_Log
{
  // サービス名
  const _PROVIDES = 'service.log';

  // ログ項目のデフォルト値
  const LOG_ENTRY_DEFAULTS = [
    'level' => LogLevels::Verbose,
    'source' => 'Unspecified',
    'event' => 'Unspecified',
  ];

  // 検証
  private static $find_entries_validator;
  private static $find_logs_validator;
  private static $save_entry_validator;

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    \Config::load('wi-log', true);
  }// </editor-fold>

  /**
   * ログ項目を検索する
   *
   * @access public
   * @param string log ログ名
   * @param array $criteria 検索条件
   *        string level レベル
   *        datetime date_from 日から
   *        datetime date_to まで日
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   */
  public function find_entries($log, array $criteria = [], array $options = [])// <editor-fold defaultstate="collapsed" desc="...">
  {
    $this->validate_find_entries($log, $criteria);

    $criteria['log'] = $log;
    $only_count = \Arr::get($options, 'only_count', false);
    $order_by = \Arr::get($options, 'order_by', 'date');
    $page = \Arr::get($options, 'page', 1) - 1;
    $per_page = \Arr::get($options, 'per_page', 0);
    $reverse = \Arr::get($options, 'reverse', true);

    $query = Model_Log_Entries::query();

    // 条件：日から
    if ( ! empty($criteria['date_from']))
    {
      $query->where('date', '>=', $criteria['date_from']);
    }

    unset($criteria['date_from']);

    // 条件：まで日
    if ( ! empty($criteria['date_to']))
    {
      // 1日追加（包括日付）
      $date_to = \Date::create_from_string($criteria['date_to']);
      $date_to = new \DateTime('@'.$date_to->get_timestamp());
      $date_to->add(new \DateInterval('P1D'));
      $date_to = \Date::forge($date_to->getTimestamp());
      $date_to = $date_to->format('ja');
      $query->where('date', '<', $date_to);
    }

    unset($criteria['date_to']);

    // 条件：メッセージ
    if ( ! empty($criteria['message']))
    {
      $query->where('message', 'contains', $criteria['message']);
    }

    unset($criteria['message']);

    // その他の条件
    foreach ($criteria as $key => $value)
    {
      if ( ! empty($value))
      {
        $query->where($key, $value);
      }
    }

    // 集計
    $count = $query->count();

    // 取得
    if ( ! $only_count)
    {
      $query->order_by($order_by, $reverse ? 'DESC' : 'ASC');
      $per_page > 0 and $query->rows_offset($page * $per_page);
      $per_page > 0 and $query->rows_limit($per_page);
      $rows = $query->get();

      // 配列に変換
      foreach ($rows as $key => $row) {
        $rows[$key] = $row->to_array();
      }
    }
    else
    {
      $rows = [];
    }

    return [
      'count' => $count,
      'page' => $page + 1,
      'per_page' => $per_page,
      'rows' => $rows,
      'order_by' => $order_by,
      'reverse' => $reverse,
    ];
  }// </editor-fold>

  /**
   * ログを検索する
   *
   * @access public
   * @param array $criteria 検索条件
   *        string name ログ名
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   */
  public function find_logs(array $criteria = [], array $options = [])// <editor-fold defaultstate="collapsed" desc="...">
  {
    $this->validate_find_logs($criteria);

    $only_count = \Arr::get($options, 'only_count', false);
    $order_by = \Arr::get($options, 'order_by', 'name');
    $page = \Arr::get($options, 'page', 1) - 1;
    $per_page = \Arr::get($options, 'per_page', 0);
    $reverse = \Arr::get($options, 'reverse', false);

    $query = \DB::select(['log', 'name'])->distinct()->from('log_entries');

    // その他の条件
    foreach ($criteria as $key => $value)
    {
      if ( ! empty($value))
      {
        $key = $key === 'name' ? 'log' : $key;  // 別名を変換する
        $query->where($key, $value);
      }
    }

    // 取得
    $query->order_by($order_by, $reverse ? 'DESC' : 'ASC');
    $per_page > 0 and $query->offset($page * $per_page);
    $per_page > 0 and $query->limit($per_page);
    $rows = $query->execute()->as_array();

    // 集計
    $count = \DB::count_last_query();

    return [
      'count' => $count,
      'page' => $page + 1,
      'per_page' => $per_page,
      'rows' => $only_count ? [] : $rows,
      'order_by' => $order_by,
      'reverse' => $reverse,
    ];
  }// </editor-fold>

  /**
   * ログ項目を取得する
   *
   * @access public
   * @param string $log ログ名
   * @param integer $id 項目ID
   * @return array ログ
   */
  public function get_entry($log, $id)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $entry = Model_Log_Entries::find_by_log_and_id($log, $id);
    return $entry ? $entry->to_array() : false;
  }// </editor-fold>

  /**
   * ログを剪定する
   *
   * @access public
   * @param string $log ログ名
   * @param mixed $config 設定
   * @return array ログ
   */
  public function prune($log, $config = 'wi-log')// <editor-fold defaultstate="collapsed" desc="...">
  {
    is_string($config) and $config = \Config::get($config, []);
    is_array($config) and $config = array_merge(\Config::get('wi-log', []), $config);

    // 過剰なログ項目を削除する
    $max_entries = +\Arr::get($config, 'maintenance.max_entries');

    if ($max_entries >= 0)
    {
      $limit = Model_Log_Entries::query()
        ->where('log', $log)
        ->order_by('id', 'desc')
        ->rows_offset($max_entries)
        ->rows_limit(1)
        ->get();

      $limit = reset($limit);
      $limit and \DB::delete('log_entries')
        ->where('log', $log)
        ->where('id', '<', $limit->id + 1)
        ->execute();
    }

    // 古いログ項目を削除する
    $max_age = +\Arr::get($config, 'maintenance.max_age');

    if ($max_age >= 0)
    {
      $max_date = new \DateTime();
      $max_date->sub(new \DateInterval('P'.$max_age.'D'));
      $max_date = \Date::forge($max_date->getTimestamp());
      $max_date = $max_date->format('mysql_date');

      \DB::delete('log_entries')
        ->where('log', $log)
        ->where('date', '<', $max_date)
        ->execute();
    }
  }// </editor-fold>

  /**
   * ログ項目を保存する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   * @return array ログ項目
   */
  public function save_entry($log, array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    empty($properties['source']) and $properties['source'] = \Config::get('wi-log.source');
    $properties = array_merge(self::LOG_ENTRY_DEFAULTS, $properties);
    $this->validate_save_entry($log, $properties);

    $data = \Arr::get($properties, 'data', null);

    if (is_object($data))
    {
      $data = @serialize($data);
    }

    $entry = Model_Log_Entries::forge($properties);
    $entry->log = $log;
    $entry->data = json_encode($data, JSON_UNESCAPED_UNICODE);
    $entry->save();

    return $entry->to_array();
  }// </editor-fold>

  /**
   * ログ項目の検索プロパティを検証する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date_start 日から
   *        datetime date_end まで日
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  public function validate_find_entries($log, array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $properties['log'] = $log;

    if (!isset(static::$find_entries_validator))
    {
      $v = static::$find_entries_validator = \Validation::forge(get_class().'_Find_Entries_Validator');
      $v->add_field('log', 'ログ名', 'trim|required|max_length[255]|valid_string[alpha,utf8,numeric,forwardslashes]');
      $v->add_field('level', 'レベル', 'trim|match_one[warning;verbose;information;error;critical]');
      $v->add_field('date_start', '開始日', 'trim|valid_date[Y/m/d H:i:s]');
      $v->add_field('date_end', '終了日', 'trim|valid_date[Y/m/d H:i:s]|valid_end_date[date_start]');
      $v->add_field('user_id', 'ユーザーID', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('source', 'ソース名', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('event', 'イベント名', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('result', '結果', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('message', 'メッセージ', 'trim|max_length[255]');
    }

    if (!static::$find_entries_validator->run($properties))
    {
      throw new ValidationError(static::$find_entries_validator->error_message());
    }
  }// </editor-fold>

  /**
   * ログの検索プロパティを検証する
   *
   * @access public
   * @param array $properties プロパティ
   *        string name ログ名
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  public function validate_find_logs(array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (!isset(static::$find_logs_validator))
    {
      $v = static::$find_logs_validator = \Validation::forge(get_class().'_Find_Logs_Validator');
      $v->add_field('name', 'ログ名', 'trim|max_length[255]|valid_string[alpha,utf8,numeric,forwardslashes]');
    }

    if (!static::$find_logs_validator->run($properties))
    {
      throw new ValidationError(static::$find_logs_validator->error_message());
    }
  }// </editor-fold>

  /**
   * ログ項目保存のプロパティを検証する
   *
   * @access public
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  public function validate_save_entry($log, array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $properties['log'] = $log;
    empty($properties['source']) and $properties['source'] = \Config::get('wi-log.source');

    if (!isset(static::$save_entry_validator))
    {
      $v = static::$save_entry_validator = \Validation::forge(get_class().'_Save_Entry_Validator');
      $v->add_field('log', 'ログ名', 'trim|required|max_length[255]|valid_string[alpha,utf8,numeric,forwardslashes]');
      $v->add_field('level', 'レベル', 'trim|match_one[warning;verbose;information;error;critical]');
      $v->add_field('date', '日時', 'trim|valid_date[Y/m/d H:i:s]');
      $v->add_field('user_id', 'ユーザーID', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('source', 'ソース名', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('event', 'イベント名', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('result', '結果', 'trim|max_length[255]|valid_string[alpha_numeric]');
      $v->add_field('message', 'メッセージ', 'trim|required|max_length[255]');
    }

    if (!static::$save_entry_validator->run($properties))
    {
      throw new ValidationError(static::$save_entry_validator->error_message());
    }
  }// </editor-fold>

  /**
   * 設定のログレベルによると、ログ項目を追加する
   *
   * @access protected
   * @param string $log ログ名
   * @param array $properties プロパティ
   *        string level レベル
   *        datetime date 日時
   *        integer user_id ユーザーID
   *        string source ソース名
   *        string event イベント名
   *        string result 結果
   *        string message メッセージ
   *        mixed data データ
   */
  public function write($log, array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $levels = [LogLevels::Critical, LogLevels::Error, LogLevels::Warn, LogLevels::Info, LogLevels::Verbose];
    $levels = array_slice($levels, 0, array_search(\Config::get('wi-log.level'), $levels) + 1);

    if (array_search(\Arr::get($properties, 'level'), $levels))
    {
      $this->save_entry($log, $properties);
    }
  }// </editor-fold>
}
